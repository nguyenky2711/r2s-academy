// import { configureStore } from '@reduxjs/toolkit';

// const store = configureStore({
//     reducer: {
//         profile: { name: 'NTK' }
//     },
//     middleware: (getDefaultMiddleware) =>
//         getDefaultMiddleware({
//             serializableCheck: false,
//         }),
// });

// export default store;
// store.js
import { configureStore } from '@reduxjs/toolkit';

const initialState = {
    profile: { name: 'NTK' },
    // Các slice và giá trị khởi tạo cho slice khác có thể được thêm vào đây nếu bạn có nhiều slices.
};

const rootReducer = (state = initialState, action) => {
    // Xử lý action và cập nhật state cho các slice ở đây nếu cần thiết.
    // Chúng ta không thực hiện bất kỳ xử lý nào trong ví dụ này vì reducer đơn giản chỉ trả về state hiện tại.
    return state;
};

const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
});

export default store;
