import React, { useState } from "react";
import { Outlet, useLocation } from "react-router-dom";
// import { Drawer, Hidden } from '@mui/material';
import { useSelector } from "react-redux";

import "./style.scss";

const ClassManagerLayout = () => {
  //   const [openDrawer, setOpenDrawer] = useState(false);
  //   const notification = useSelector((state) => state.notification);

  //   const handleCloseDrawer = () => {
  //     setOpenDrawer(false);
  //   };

  return (
    <div className="classManager-layout">
      <div className="classManager-layout__sidebar">
        {/* <Hidden mdDown>
          <Drawer
            variant='persistent'
            className='admin-layout__sidebar-block'
            open
          >
            <Sidebar />
          </Drawer>
        </Hidden>
        <Drawer
          variant='temporary'
          anchor='left'
          open={openDrawer}
          onClose={handleCloseDrawer}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          className='admin-layout__sidebar-hide'
        >
          <Sidebar />
        </Drawer> */}
      </div>
      <div className="classManager-layout__content">
        <header className="classManager-layout__header">
          header admin
          {/* <AdminNav openDrawer={openDrawer} setOpenDrawer={setOpenDrawer} /> */}
        </header>
        <main className="classManager-layout__main">
          <section>
            <Outlet />
          </section>
        </main>
      </div>
      {/* <Notification notifyAlert={notification} /> */}
    </div>
  );
};

export default ClassManagerLayout;
