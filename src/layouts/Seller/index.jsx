import React, { useState } from "react";
import { Outlet, useLocation } from "react-router-dom";
// import { Drawer, Hidden } from '@mui/material';
import { useSelector } from "react-redux";

import "./style.scss";

const SellerLayout = () => {
  //   const [openDrawer, setOpenDrawer] = useState(false);
  //   const notification = useSelector((state) => state.notification);

  //   const handleCloseDrawer = () => {
  //     setOpenDrawer(false);
  //   };

  return (
    <div className="seller-layout">
      <div className="seller-layout__sidebar">
        {/* <Hidden mdDown>
          <Drawer
            variant='persistent'
            className='admin-layout__sidebar-block'
            open
          >
            <Sidebar />
          </Drawer>
        </Hidden>
        <Drawer
          variant='temporary'
          anchor='left'
          open={openDrawer}
          onClose={handleCloseDrawer}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          className='admin-layout__sidebar-hide'
        >
          <Sidebar />
        </Drawer> */}
      </div>
      <div className="seller-layout__content">
        <header className="seller-layout__header">
          header seller
          {/* <AdminNav openDrawer={openDrawer} setOpenDrawer={setOpenDrawer} /> */}
        </header>
        <main className="seller-layout__main">
          <section>
            <Outlet />
          </section>
        </main>
      </div>
      {/* <Notification notifyAlert={notification} /> */}
    </div>
  );
};

export default SellerLayout;
