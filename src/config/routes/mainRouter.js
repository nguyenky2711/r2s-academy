
import HomePage from '../../features/General/Main/Pages/Home';
import LoginPage from '../../features/General/WorkingSession/Pages/Login';
import RegisterPage from '../../features/General/WorkingSession/Pages/Register';
import MainLayout from '../../layouts/Main';
const mainRouter = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        path: '',
        Component: HomePage,
      },
      // {
      //   path: '/detail_job/:keyword',
      //   Component: DetailHome,
      // },
      // {
      //   path: '/information_company/:keyword',
      //   Component: CandidateInformationCompany,
      // },
    ],
  },
  // {
  //   path: '/not-found',
  //   element: <NotFound />,
  // },
  {
    path: 'login',
    element: <LoginPage />,
  },
  {
    path: 'register',
    element: <RegisterPage />,
  },
  {
    //Doesn't match route?, It's here. If you want to change just delete children and change element
    path: '*',
    element: <HomePage />,
    children: [
      {
        path: '*',
        Component: HomePage,
      },
    ],
  },
];
export default mainRouter;
