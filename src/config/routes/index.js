import mainRouter from './mainRouter';
import adminRouter from './adminRouter';
import classManagerRouter from './classManagerRouter';
import sellerRouter from './sellerRouter';

const publicRouter = [
  mainRouter,

];
const privateRouter = [adminRouter, classManagerRouter, sellerRouter];
export { publicRouter, privateRouter };
