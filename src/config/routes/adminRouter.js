
import AdminLayout from '../../layouts/Admin/index.jsx';
import CommissionForm from '../../features/Admin/CommissionManger/Pages/CommissionForm';
import CommissionInforIntern from '../../features/Admin/CommissionManger/Pages/CommissionInforIntern';
import CommissionInforOfficial from '../../features/Admin/CommissionManger/Pages/CommissionInforOfficial';
import CourseFormPage from '../../features/Admin/CourseManager/Pages/CourseForm';
import CourseInforPage from '../../features/Admin/CourseManager/Pages/CourseInfor';
import DiscountPolicyForm from '../../features/Admin/CourseManager/Pages/DiscountPolicyForm';
import DiscountPolicyInfor from '../../features/Admin/CourseManager/Pages/DiscountPolicyInfor';
import EnrollmentProcessForm from '../../features/Admin/CourseManager/Pages/EnrollmentProcessForm';
import EnrollmentProcessInfor from '../../features/Admin/CourseManager/Pages/EnrollmentProcessInfor';
import RegisterProcessForm from '../../features/Admin/CourseManager/Pages/RegisterProcessForm';
import RegisterProcessInfor from '../../features/Admin/CourseManager/Pages/RegisterProcessInfor';

const adminRouter = [
  {
    role: 'Role_Admin',
    path: '/admin',
    element: <AdminLayout />,
    // index: <Dashboard />,
    children: [
      {
        path: 'courseForm',
        Component: <CourseFormPage />,
      },
      {
        path: 'courseInfor',
        Component: <CourseInforPage />,
      },
      {
        path: 'discountForm',
        Component: <DiscountPolicyForm />,
      },
      {
        path: 'discountInfor',
        Component: <DiscountPolicyInfor />,
      },
      {
        path: 'enrollmentForm',
        Component: <EnrollmentProcessForm />,
      },
      {
        path: 'enrollmentInfor',
        Component: <EnrollmentProcessInfor />,
      },
      {
        path: 'registerProcessForm',
        Component: <RegisterProcessForm />,
      },
      {
        path: 'registerProcessInfor',
        Component: <RegisterProcessInfor />,
      },
      {
        path: 'commissionForm',
        Component: <CommissionForm />,
      },
      {
        path: 'commissionIntern',
        Component: <CommissionInforIntern />,
      },
      {
        path: 'commissionOfficial',
        Component: <CommissionInforOfficial />,
      },
      // {
      //   path: '*',
      //   Component: NotFound,
      // },
    ],
  },
];
export default adminRouter;
