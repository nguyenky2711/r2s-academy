
import ClassManagerLayout from '../../layouts/ClassManager';
import ClassInforPage from '../../features/ClassManager/Pages/ClassInfor';

const classManagerRouter = [
  {
    role: 'Role_ClassManager',
    path: '/classManager',
    element: <ClassManagerLayout />,
    children: [
      {
        path: 'classInfor',
        Component: <ClassInforPage />,
      },
    ],
  },
];

export default classManagerRouter;
