
import SellerLayout from '../../layouts/Seller';
import ReportInfor from '../../features/Seller/Pages/ReportInfor';
import WeeklyReportForm from '../../features/Seller/Pages/WeeklyReportForm';
import CandidateRegisterForm from '../../features/Seller/Pages/CandidateRegisterForm';

const sellerRouter = [
  {
    role: 'Role_Seller',
    path: '/seller',
    element: <SellerLayout />,
    children: [
      {
        path: 'reportInfor',
        Component: <ReportInfor />,
      },
      {
        path: 'reportForm',
        Component: <WeeklyReportForm />,
      },
      {
        path: 'candidateRegisterForm',
        Component: <CandidateRegisterForm />,
      },

    ],
  },
];

export default sellerRouter;
